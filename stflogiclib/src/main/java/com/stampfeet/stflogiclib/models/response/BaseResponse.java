package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFStatusEntity;


public class BaseResponse {

    @SerializedName("status")
    private SFStatusEntity status;

    public SFStatusEntity getStatus() {
        return status;
    }

    public void setStatus(SFStatusEntity status) {
        this.status = status;
    }
}
