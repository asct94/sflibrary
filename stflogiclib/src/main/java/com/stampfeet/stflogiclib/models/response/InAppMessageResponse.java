package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFMessageEntity;


public class InAppMessageResponse extends BaseResponse {

    @SerializedName("message")
    private SFMessageEntity message;

    public SFMessageEntity getMessage() {
        return message;
    }

    public void setMessage(SFMessageEntity message) {
        this.message = message;
    }
}
