package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;

public class MessageToBusinessRequest {

    @SerializedName("msg")
    private String msg;
    @SerializedName("to")
    private String to;
    @SerializedName("subject")
    private String subject;
    @SerializedName("userId")
    private int userid;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }
}
