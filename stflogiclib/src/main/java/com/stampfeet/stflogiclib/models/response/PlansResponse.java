package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFPlanEntity;

import java.util.ArrayList;


public class PlansResponse extends BaseResponse {

    @SerializedName("plans_list")
    private ArrayList<SFPlanEntity> plans;

    public ArrayList<SFPlanEntity> getPlans() {
        return plans;
    }

    public void setPlans(ArrayList<SFPlanEntity> plans) {
        this.plans = plans;
    }
}
