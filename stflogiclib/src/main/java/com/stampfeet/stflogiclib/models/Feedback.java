package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

public class Feedback {

    @SerializedName("cardId")
    private int cardid;
    @SerializedName("textualReason")
    private String textualreason;
    @SerializedName("text")
    private String text;
    @SerializedName("rate")
    private double rate;
    @SerializedName("stampId")
    private int stampid;
    @SerializedName("branchId")
    private int branchid;
    @SerializedName("businessId")
    private int businessid;
    @SerializedName("userId")
    private int userid;

    public int getCardid() {
        return cardid;
    }

    public void setCardid(int cardid) {
        this.cardid = cardid;
    }

    public String getTextualreason() {
        return textualreason;
    }

    public void setTextualreason(String textualreason) {
        this.textualreason = textualreason;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getStampid() {
        return stampid;
    }

    public void setStampid(int stampid) {
        this.stampid = stampid;
    }

    public int getBranchid() {
        return branchid;
    }

    public void setBranchid(int branchid) {
        this.branchid = branchid;
    }

    public int getBusinessid() {
        return businessid;
    }

    public void setBusinessid(int businessid) {
        this.businessid = businessid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }
}
