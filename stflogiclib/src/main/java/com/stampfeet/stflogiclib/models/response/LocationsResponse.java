package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFLocationPageEntity;


public class LocationsResponse extends BaseResponse {

    @SerializedName("page")
    private SFLocationPageEntity locationPage;

    public SFLocationPageEntity getLocationPage() {
        return locationPage;
    }

    public void setLocationPage(SFLocationPageEntity locationPage) {
        this.locationPage = locationPage;
    }
}
