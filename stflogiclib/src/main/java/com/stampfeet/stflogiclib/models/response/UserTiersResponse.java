package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFLoyaltyTierEntity;

import java.util.ArrayList;


public class UserTiersResponse extends BaseResponse {

    @SerializedName("loyalty_tiers")
    ArrayList<SFLoyaltyTierEntity> loyaltyTiers;

    public ArrayList<SFLoyaltyTierEntity> getLoyaltyTiers() {
        return loyaltyTiers;
    }

    public void setLoyaltyTiers(ArrayList<SFLoyaltyTierEntity> loyaltyTiers) {
        this.loyaltyTiers = loyaltyTiers;
    }
}
