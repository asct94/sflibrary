package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFMessageEntity {

    @SerializedName("img_link")
    private String imgLink;
    @SerializedName("link")
    private String link;
    @SerializedName("actionButtonLabel")
    private String actionButtonLabel;
    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("business_id")
    private int businessId;
    @SerializedName("content")
    private String content;
    @SerializedName("live")
    private boolean live;

    public String getImgLink() {
        return imgLink;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getActionButtonLabel() {
        return actionButtonLabel;
    }

    public void setActionButtonLabel(String actionButtonLabel) {
        this.actionButtonLabel = actionButtonLabel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }
}
