package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFPromotionEntity {

    @SerializedName("id")
    private int id;
    @SerializedName("description")
    private String despcription;
    @SerializedName("title")
    private String title;
    @SerializedName("image_1")
    private String image1;
    @SerializedName("image_2")
    private String image2;
    @SerializedName("subtitle_1")
    private String subtitle1;
    @SerializedName("subtitle_2")
    private String subtitle2;
    @SerializedName("to_date")
    private String toDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDespcription() {
        return despcription;
    }

    public void setDespcription(String despcription) {
        this.despcription = despcription;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getSubtitle1() {
        return subtitle1;
    }

    public void setSubtitle1(String subtitle1) {
        this.subtitle1 = subtitle1;
    }

    public String getSubtitle2() {
        return subtitle2;
    }

    public void setSubtitle2(String subtitle2) {
        this.subtitle2 = subtitle2;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
