package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFRewardEntity {

    @SerializedName("image_1")
    private String image1;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private int id;
    @SerializedName("image_2")
    private String image2;

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }
}
