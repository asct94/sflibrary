package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("ok")
    private boolean ok;
    @SerializedName("userLoggedIn")
    private boolean userloggedin;
    @SerializedName("statusDescription")
    private String statusdescription;
    @SerializedName("statusCode")
    private int statuscode;

    public boolean getOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public boolean getUserloggedin() {
        return userloggedin;
    }

    public void setUserloggedin(boolean userloggedin) {
        this.userloggedin = userloggedin;
    }

    public String getStatusdescription() {
        return statusdescription;
    }

    public void setStatusdescription(String statusdescription) {
        this.statusdescription = statusdescription;
    }

    public int getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(int statuscode) {
        this.statuscode = statuscode;
    }
}
