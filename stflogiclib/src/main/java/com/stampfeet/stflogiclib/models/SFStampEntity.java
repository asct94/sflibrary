package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFStampEntity {

    @SerializedName("stamp_id")
    private int stampId;

    public int getStampId() {
        return stampId;
    }

    public void setStampId(int stampId) {
        this.stampId = stampId;
    }
}
