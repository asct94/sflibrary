package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFDynamicBonusEntity;
import com.stampfeet.stflogiclib.models.SFStampEntity;

import java.util.ArrayList;


public class CreditUpdateResponse extends BaseResponse {

    @SerializedName("dynamicbonus")
    private ArrayList<SFDynamicBonusEntity> dynamicBonuses;
    @SerializedName("stamp")
    private SFStampEntity stamp;
    @SerializedName("current_stamps")
    private int currentStamps;

    public ArrayList<SFDynamicBonusEntity> getDynamicBonuses() {
        return dynamicBonuses;
    }

    public void setDynamicBonuses(ArrayList<SFDynamicBonusEntity> dynamicBonuses) {
        this.dynamicBonuses = dynamicBonuses;
    }

    public SFStampEntity getStamp() {
        return stamp;
    }

    public void setStamp(SFStampEntity stamp) {
        this.stamp = stamp;
    }

    public int getCurrentStamps() {
        return currentStamps;
    }

    public void setCurrentStamps(int currentStamps) {
        this.currentStamps = currentStamps;
    }
}

