package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class SFPropertiesEntity {

    @SerializedName("Feedback_options_list")
    private ArrayList<SFFeedbackOptionEntity> feedbackOptions;

    public ArrayList<SFFeedbackOptionEntity> getFeedbackOptions() {
        return feedbackOptions;
    }

    public void setFeedbackOptions(ArrayList<SFFeedbackOptionEntity> feedbackOptions) {
        this.feedbackOptions = feedbackOptions;
    }
}
