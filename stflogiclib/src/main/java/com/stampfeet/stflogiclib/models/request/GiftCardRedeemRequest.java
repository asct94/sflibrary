package com.stampfeet.stflogiclib.models.request;

import com.google.gson.annotations.SerializedName;

public class GiftCardRedeemRequest {

    @SerializedName("giftCode")
    private int giftcode;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("latitude")
    private double latitude;

    public int getGiftcode() {
        return giftcode;
    }

    public void setGiftcode(int giftcode) {
        this.giftcode = giftcode;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
