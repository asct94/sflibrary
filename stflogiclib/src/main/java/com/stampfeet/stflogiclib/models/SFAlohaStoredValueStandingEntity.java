package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class SFAlohaStoredValueStandingEntity {

    @SerializedName("id") private String id;
    @SerializedName("currentBalance") private double currentBalance;
    @SerializedName("eligibleReward") private ArrayList<SFAlohaEligibleRewardEntity> eligibleRewards;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public ArrayList<SFAlohaEligibleRewardEntity> getEligibleRewards() {
        return eligibleRewards;
    }

    public void setEligibleRewards(ArrayList<SFAlohaEligibleRewardEntity> eligibleRewards) {
        this.eligibleRewards = eligibleRewards;
    }
}
