package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class SFPlanEntity {

    @SerializedName("id")
    private int id;
    @SerializedName("card")
    private SFCardEntity card;
    @SerializedName("dynamicbonus")
    private ArrayList<SFDynamicBonusEntity> dynamicBonuses;
    @SerializedName("fixedbonus")
    private ArrayList<SFFixedBonusEntity> fixedBonuses;
    @SerializedName("card_id")
    private int cardId;
    @SerializedName("current_points")
    private int currentPoints;
    @SerializedName("current_stamps")
    private int currentStamps;
    @SerializedName("total_spend")
    private int totalSpend;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SFCardEntity getCard() {
        return card;
    }

    public void setCard(SFCardEntity card) {
        this.card = card;
    }

    public ArrayList<SFDynamicBonusEntity> getDynamicBonuses() {
        return dynamicBonuses;
    }

    public void setDynamicBonuses(ArrayList<SFDynamicBonusEntity> dynamicBonuses) {
        this.dynamicBonuses = dynamicBonuses;
    }

    public ArrayList<SFFixedBonusEntity> getFixedBonuses() {
        return fixedBonuses;
    }

    public void setFixedBonuses(ArrayList<SFFixedBonusEntity> fixedBonuses) {
        this.fixedBonuses = fixedBonuses;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getCurrentPoints() {
        return currentPoints;
    }

    public void setCurrentPoints(int currentPoints) {
        this.currentPoints = currentPoints;
    }

    public int getCurrentStamps() {
        return currentStamps;
    }

    public void setCurrentStamps(int currentStamps) {
        this.currentStamps = currentStamps;
    }

    public int getTotalSpend() {
        return totalSpend;
    }

    public void setTotalSpend(int totalSpend) {
        this.totalSpend = totalSpend;
    }
}
