package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFErrorDetailEntity {

    @SerializedName("id")
    private int id;
    @SerializedName("detail")
    private String detail;

    public SFErrorDetailEntity(int id, String detail){
        this.id = id;
        this.detail = detail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
