package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFDynamicBonusEntity {

    @SerializedName("active")
    private boolean active;
    @SerializedName("cardId")
    private int cardId;
    @SerializedName("currentStampsMax")
    private int currentStampMax;
    @SerializedName("currentStampsMin")
    private int currentStampMin;
    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("nbOfDaySinceLastVisit")
    private String nbOfDaySinceLastVisit;
    @SerializedName("totalVisitsMax")
    private int totalVisitsMax;
    @SerializedName("totalVisitsMin")
    private int totalVisitMin;
    @SerializedName("image_1")
    private String image1;
    @SerializedName("image_2")
    private String image2;
    @SerializedName("num_of_stamps")
    private int numOfStamps;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getCurrentStampMax() {
        return currentStampMax;
    }

    public void setCurrentStampMax(int currentStampMax) {
        this.currentStampMax = currentStampMax;
    }

    public int getCurrentStampMin() {
        return currentStampMin;
    }

    public void setCurrentStampMin(int currentStampMin) {
        this.currentStampMin = currentStampMin;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNbOfDaySinceLastVisit() {
        return nbOfDaySinceLastVisit;
    }

    public void setNbOfDaySinceLastVisit(String nbOfDaySinceLastVisit) {
        this.nbOfDaySinceLastVisit = nbOfDaySinceLastVisit;
    }

    public int getTotalVisitsMax() {
        return totalVisitsMax;
    }

    public void setTotalVisitsMax(int totalVisitsMax) {
        this.totalVisitsMax = totalVisitsMax;
    }

    public int getTotalVisitMin() {
        return totalVisitMin;
    }

    public void setTotalVisitMin(int totalVisitMin) {
        this.totalVisitMin = totalVisitMin;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public int getNumOfStamps() {
        return numOfStamps;
    }

    public void setNumOfStamps(int numOfStamps) {
        this.numOfStamps = numOfStamps;
    }
}
