package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class SFAlohaUserPlanEntity {

    @SerializedName("id") private String id;
    @SerializedName("userId") private int userId;
    @SerializedName("cardNumber") private String cardNumber;
    @SerializedName("loyaltyPlans") private ArrayList<SFAlohaLoyaltyPlanEntity> loyaltyPlans;
    @SerializedName("storedvalueStandings") private SFAlohaStoredValueStandingEntity storedValueStandings;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public ArrayList<SFAlohaLoyaltyPlanEntity> getLoyaltyPlans() {
        return loyaltyPlans;
    }

    public void setLoyaltyPlans(ArrayList<SFAlohaLoyaltyPlanEntity> loyaltyPlans) {
        this.loyaltyPlans = loyaltyPlans;
    }

    public SFAlohaStoredValueStandingEntity getStoredValueStandings() {
        return storedValueStandings;
    }

    public void setStoredValueStandings(SFAlohaStoredValueStandingEntity storedValueStandings) {
        this.storedValueStandings = storedValueStandings;
    }
}
