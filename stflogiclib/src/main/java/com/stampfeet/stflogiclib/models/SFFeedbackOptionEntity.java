package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFFeedbackOptionEntity {

    @SerializedName("index")
    private int index;
    @SerializedName("name")
    private String name;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
