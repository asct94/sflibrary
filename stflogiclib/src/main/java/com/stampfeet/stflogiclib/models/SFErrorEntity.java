package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFErrorEntity {

    @SerializedName("errors")
    private SFErrorDetailEntity errorDetail;

    public SFErrorDetailEntity getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(SFErrorDetailEntity errorDetail) {
        this.errorDetail = errorDetail;
    }
}
