package com.stampfeet.stflogiclib.models.response;

import com.google.gson.annotations.SerializedName;
import com.stampfeet.stflogiclib.models.SFBannerEntity;

import java.util.ArrayList;


public class BannersResponse extends BaseResponse {

    @SerializedName("welcome_message_list")
    private ArrayList<SFBannerEntity> banners;

    public ArrayList<SFBannerEntity> getBanners() {
        return banners;
    }

    public void setBanners(ArrayList<SFBannerEntity> banners) {
        this.banners = banners;
    }
}
