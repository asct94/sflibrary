package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class SFBranchEntity {

    @SerializedName("id")
    private int id;
    @SerializedName("branch_image_url")
    private String branchImageUrl;
    @SerializedName("leaving_region_message")
    private String leavingRegionMessage;
    @SerializedName("website")
    private String website;
    @SerializedName("address")
    private SFAddressEntity address;
    @SerializedName("entering_region_message")
    private String enteringRegionMessage;
    @SerializedName("branch_hours")
    private String branchHours;
    @SerializedName("website_1")
    private String website1;
    @SerializedName("website_2")
    private String website2;
    @SerializedName("website_3")
    private String website3;
    @SerializedName("branch_description")
    private String branchDecription;
    @SerializedName("branch_id")
    private int branchId;
    @SerializedName("branch_name")
    private String branchName;
    @SerializedName("opening_hours")
    private ArrayList<SFHoursEntity> hours;
    @SerializedName("branch_phone_number")
    private String branchPhoneNumber;
    @SerializedName("breakfast_hours")
    private String breakfastHours;
    @SerializedName("category_a")
    private String categoryA;
    @SerializedName("category_b")
    private String categoryB;
    @SerializedName("fixedbonus")
    private ArrayList<SFFixedBonusEntity> fixedBonus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBranchImageUrl() {
        return branchImageUrl;
    }

    public void setBranchImageUrl(String btanchImageUrl) {
        this.branchImageUrl = btanchImageUrl;
    }

    public String getLeavingRegionMessage() {
        return leavingRegionMessage;
    }

    public void setLeavingRegionMessage(String leavingRegionMessage) {
        this.leavingRegionMessage = leavingRegionMessage;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public SFAddressEntity getAddress() {
        return address;
    }

    public void setAddress(SFAddressEntity address) {
        this.address = address;
    }

    public String getEnteringRegionMessage() {
        return enteringRegionMessage;
    }

    public void setEnteringRegionMessage(String enteringRegionMessage) {
        this.enteringRegionMessage = enteringRegionMessage;
    }

    public String getBranchHours() {
        return branchHours;
    }

    public void setBranchHours(String branchHours) {
        this.branchHours = branchHours;
    }

    public String getWebsite1() {
        return website1;
    }

    public void setWebsite1(String website1) {
        this.website1 = website1;
    }

    public String getWebsite2() {
        return website2;
    }

    public void setWebsite2(String website2) {
        this.website2 = website2;
    }

    public String getBranchDecription() {
        return branchDecription;
    }

    public void setBranchDecription(String branchDecription) {
        this.branchDecription = branchDecription;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public ArrayList<SFHoursEntity> getHours() {
        return hours;
    }

    public void setHours(ArrayList<SFHoursEntity> hours) {
        this.hours = hours;
    }

    public String getBranchPhoneNumber() {
        return branchPhoneNumber;
    }

    public void setBranchPhoneNumber(String branchPhoneNumber) {
        this.branchPhoneNumber = branchPhoneNumber;
    }

    public String getBreakfastHours() {
        return breakfastHours;
    }

    public void setBreakfastHours(String breakfastHours) {
        this.breakfastHours = breakfastHours;
    }

    public String getCategoryA() {
        return categoryA;
    }

    public void setCategoryA(String categoryA) {
        this.categoryA = categoryA;
    }

    public String getCategoryB() {
        return categoryB;
    }

    public void setCategoryB(String categoryB) {
        this.categoryB = categoryB;
    }

    public ArrayList<SFFixedBonusEntity> getFixedBonus() {
        return fixedBonus;
    }

    public void setFixedBonus(ArrayList<SFFixedBonusEntity> fixedBonus) {
        this.fixedBonus = fixedBonus;
    }

    public String getWebsite3() {
        return website3;
    }

    public void setWebsite3(String website3) {
        this.website3 = website3;
    }
}
