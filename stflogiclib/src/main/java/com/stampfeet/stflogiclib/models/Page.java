package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Page<T> {

    @SerializedName("number_of_elements")
    private int numberOfElements;
    @SerializedName("total_number_of_elements")
    private int totalNumberOfElements;
    @SerializedName("has_prev")
    private boolean hasPrev;
    @SerializedName("has_next")
    private boolean hasNext;
    @SerializedName("is_first")
    private boolean isFirst;
    @SerializedName("total_pages_number")
    private int totalPagesNumber;
    @SerializedName("content_list")
    private List<T> contentList;
    @SerializedName("page_number")
    private int pageNumber;
    @SerializedName("is_last")
    private boolean isLast;

    public int getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(int numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public int getTotalNumberOfElements() {
        return totalNumberOfElements;
    }

    public void setTotalNumberOfElements(int totalNumberOfElements) {
        this.totalNumberOfElements = totalNumberOfElements;
    }

    public boolean getHasPrev() {
        return hasPrev;
    }

    public void setHasPrev(boolean hasPrev) {
        this.hasPrev = hasPrev;
    }

    public boolean getHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public boolean getIsFirst() {
        return isFirst;
    }

    public void setIsFirst(boolean isFirst) {
        this.isFirst = isFirst;
    }

    public int getTotalPagesNumber() {
        return totalPagesNumber;
    }

    public void setTotalPagesNumber(int totalPagesNumber) {
        this.totalPagesNumber = totalPagesNumber;
    }

    public List<T> getContentList() {
        return contentList;
    }

    public void setContentList(List<T> contentList) {
        this.contentList = contentList;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(boolean isLast) {
        this.isLast = isLast;
    }
}
