package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class SFLocationPageEntity {

    @SerializedName("page_number")
    private int pageNumber;
    @SerializedName("is_last")
    private boolean isLast;
    @SerializedName("total_pages_number")
    private int totalPagesNumber;
    @SerializedName("content_list")
    private ArrayList<SFBranchEntity> listBranches;

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public boolean isLast() {
        return isLast;
    }

    public void setLast(boolean last) {
        isLast = last;
    }

    public int getTotalPagesNumber() {
        return totalPagesNumber;
    }

    public void setTotalPagesNumber(int totalPagesNumber) {
        this.totalPagesNumber = totalPagesNumber;
    }

    public ArrayList<SFBranchEntity> getListBranches() {
        return listBranches;
    }

    public void setListBranches(ArrayList<SFBranchEntity> listBranches) {
        this.listBranches = listBranches;
    }
}
