package com.stampfeet.stflogiclib.models;

import com.stampfeet.stflogiclib.models.response.BaseResponse;

import java.util.ArrayList;
import java.util.List;


public class InitialLoadBundle extends BaseResponse {

    private List<SFLoyaltyTierEntity> loyaltyTiers;
    private List<SFPlanEntity> plans;
    private SFMessageEntity message;

    public InitialLoadBundle() {
        loyaltyTiers = new ArrayList<>();
        plans = new ArrayList<>();
        message = null;
    }

    public List<SFLoyaltyTierEntity> getLoyaltyTiers() {
        return loyaltyTiers;
    }

    public void setLoyaltyTiers(List<SFLoyaltyTierEntity> loyaltyTiers) {
        this.loyaltyTiers = loyaltyTiers;
    }

    public List<SFPlanEntity> getPlans() {
        return plans;
    }

    public void setPlans(List<SFPlanEntity> plans) {
        this.plans = plans;
    }

    public SFMessageEntity getMessage() {
        return message;
    }

    public void setMessage(SFMessageEntity message) {
        this.message = message;
    }
}
