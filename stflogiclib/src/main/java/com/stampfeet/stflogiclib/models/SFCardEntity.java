package com.stampfeet.stflogiclib.models;

import com.google.gson.annotations.SerializedName;


public class SFCardEntity {

    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("terms")
    private String terms;
    @SerializedName("reward")
    private String reward;
    @SerializedName("code")
    private int code;
    @SerializedName("style")
    private String style;
    @SerializedName("card_index")
    private int cardIndex;
    @SerializedName("business_id")
    private int businessId;
    @SerializedName("branch_id")
    private int branchId;
    @SerializedName("display_name")
    private String displayName;
    @SerializedName("stamps_required")
    private int stampsRequired;
    @SerializedName("background_image_url")
    private String backgroundImageURL;
    @SerializedName("initial_stamps")
    private int initialStamps;
    @SerializedName("points_required")
    private int pointsRequired;
    @SerializedName("points_factor")
    private int pointsFactor;
    @SerializedName("card_type")
    private String cardType;
    @SerializedName("mid_reward_active")
    private boolean midRewardActive;
    @SerializedName("mid_reward_stamps")
    private int midRewardStamps;
    @SerializedName("mid_reward_label")
    private String midRewardLabel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public int getCardIndex() {
        return cardIndex;
    }

    public void setCardIndex(int cardIndex) {
        this.cardIndex = cardIndex;
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getStampsRequired() {
        return stampsRequired;
    }

    public void setStampsRequired(int stampsRequired) {
        this.stampsRequired = stampsRequired;
    }

    public String getBackgroundImageURL() {
        return backgroundImageURL;
    }

    public void setBackgroundImageURL(String backgroundImageURL) {
        this.backgroundImageURL = backgroundImageURL;
    }

    public int getInitialStamps() {
        return initialStamps;
    }

    public void setInitialStamps(int initialStamps) {
        this.initialStamps = initialStamps;
    }

    public int getPointsRequired() {
        return pointsRequired;
    }

    public void setPointsRequired(int pointsRequired) {
        this.pointsRequired = pointsRequired;
    }

    public int getPointsFactor() {
        return pointsFactor;
    }

    public void setPointsFactor(int pointsFactor) {
        this.pointsFactor = pointsFactor;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public boolean isMidRewardActive() {
        return midRewardActive;
    }

    public void setMidRewardActive(boolean midRewardActive) {
        this.midRewardActive = midRewardActive;
    }

    public int getMidRewardStamps() {
        return midRewardStamps;
    }

    public void setMidRewardStamps(int midRewardStamps) {
        this.midRewardStamps = midRewardStamps;
    }

    public String getMidRewardLabel() {
        return midRewardLabel;
    }

    public void setMidRewardLabel(String midRewardLabel) {
        this.midRewardLabel = midRewardLabel;
    }
}
