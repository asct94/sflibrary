package com.stampfeet.stflogiclib.source.remote;

import android.os.Build;

import com.stampfeet.stflogiclib.models.Feedback;
import com.stampfeet.stflogiclib.models.InitialLoadBundle;
import com.stampfeet.stflogiclib.models.Session;
import com.stampfeet.stflogiclib.models.User;
import com.stampfeet.stflogiclib.models.request.ChangePasswordRequest;
import com.stampfeet.stflogiclib.models.request.CreditUpdateRequest;
import com.stampfeet.stflogiclib.models.request.ForgotPasswordRequest;
import com.stampfeet.stflogiclib.models.request.GiftCardRedeemRequest;
import com.stampfeet.stflogiclib.models.request.LoginRequest;
import com.stampfeet.stflogiclib.models.request.MessageToBusinessRequest;
import com.stampfeet.stflogiclib.models.request.UpdateDeviceRequest;
import com.stampfeet.stflogiclib.models.response.InAppMessageResponse;
import com.stampfeet.stflogiclib.models.response.BannersResponse;
import com.stampfeet.stflogiclib.models.response.BaseResponse;
import com.stampfeet.stflogiclib.models.response.CreditUpdateResponse;
import com.stampfeet.stflogiclib.models.response.FeedbackPropertiesResponse;
import com.stampfeet.stflogiclib.models.response.GiftCardRedeemResponse;
import com.stampfeet.stflogiclib.models.response.LocationsResponse;
import com.stampfeet.stflogiclib.models.response.PlansResponse;
import com.stampfeet.stflogiclib.models.response.PromotionsResponse;
import com.stampfeet.stflogiclib.models.response.UserAuthResponse;
import com.stampfeet.stflogiclib.models.response.UserTiersResponse;
import com.stampfeet.stflogiclib.source.local.SessionManager;
import com.stampfeet.stflogiclib.utils.DeviceUtils;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class DataSourceRemote {

    private static final int BUSINESS_ID = 252;

    public static void init(String appKey, String baseUrl) {
        RequestManager.init(appKey, baseUrl);
    }


//    To have it updated
    private static int getUserId(){
        return SessionManager.getInstance().getSession().getUserid();
    }

    public static Observable<UserAuthResponse> signUp(User user) {
        return RequestManager.getApi().signup(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<UserAuthResponse> login(LoginRequest request) {
        return RequestManager.getApi().login(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(response -> {
                    SessionManager.getInstance().saveSession(response.getSession());
                    RequestManager.buildSecureApi(response.getSession().getSessionkey());
                });
    }

    public static void logout() {
        SessionManager.getInstance().removeSession();
        RequestManager.destroySecureApi();
    }

    public static Observable<BaseResponse> forgotPassword(ForgotPasswordRequest request) {
        return RequestManager.getApi().forgotPassword(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> changePassword(ChangePasswordRequest request) {
        return RequestManager.getSecureApi().changePassword(getUserId(), request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public static Observable<UserTiersResponse> getUserTiers() {
        return RequestManager.getSecureApi().getUserTiers(getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<InAppMessageResponse> getInAppMessage() {
        return RequestManager.getSecureApi().inAppMessage(getUserId(), BUSINESS_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<CreditUpdateResponse> creditUpdate(CreditUpdateRequest body) {
        return RequestManager.getSecureApi().creditUpdate(getUserId(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> updateDevice() {

        UpdateDeviceRequest deviceInfo = new UpdateDeviceRequest();
        deviceInfo.setPlatform("Android");
        deviceInfo.setDevice(DeviceUtils.getDeviceName());
        deviceInfo.setOsversion(Build.VERSION.RELEASE);
        deviceInfo.setAppid("1234567890");

        return RequestManager.getSecureApi().updateDevice(getUserId(), deviceInfo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> updateLocation(double latitude, double longitude) {

        HashMap<String, Object> body = new HashMap<>();
        body.put("latitude", latitude);
        body.put("longitude", longitude);

        return RequestManager.getSecureApi().updateLocation(getUserId(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<UserAuthResponse> getUser() {
        return RequestManager.getSecureApi().getUser(getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> updateUser(User user) {
        return RequestManager.getSecureApi().updateUser(user.getId(), user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BaseResponse> sendMessageToBusiness(MessageToBusinessRequest message) {
        return RequestManager.getSecureApi().sendMessageToBusiness(message)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<FeedbackPropertiesResponse> getFeedbackProperties(int businessId) {
        return RequestManager.getSecureApi().getFeedbackProperties(businessId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public static Observable<BaseResponse> addFeedback(Feedback feedback) {
        return RequestManager.getSecureApi().addFeedback(feedback)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<BannersResponse> getBanners() {
        return RequestManager.getSecureApi().getBanners(getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<LocationsResponse> getLocations(double latitude, double longitude, int pageNumber) {
        return RequestManager.getSecureApi().getLocations(latitude, longitude, pageNumber, 500, "distance", true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PlansResponse> getUserPlans() {
        return RequestManager.getSecureApi().getUserPlans(getUserId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<PromotionsResponse> getPromotions(double latitude, double longitude) {
        return RequestManager.getSecureApi().getPromotions(getUserId(), latitude, longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static Observable<GiftCardRedeemResponse> redeemGiftCard(GiftCardRedeemRequest body) {
        return RequestManager.getSecureApi().redeemGiftCard(getUserId(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public static Observable loadInitialData(double latitude, double longitude) {
        Session session = SessionManager.getInstance().getSession();
        if (session == null || session.getUserid() == null) {
            return Observable.just(new InitialLoadBundle());
        }

//        Update Endpoints
        Observable<Boolean> updateInfo = Observable.zip(
                updateDevice(),
                updateLocation(latitude, longitude),
                (br1, br2) -> br1.getStatus().getErrorCode() == 0 && br2.getStatus().getErrorCode() == 0);

//        Get Info Endpoints
        Observable<InitialLoadBundle> bundleInfo = Observable.zip(
                        getUserTiers(),
                        getUserPlans(),
                        getInAppMessage(),
                        (userTiersResponse, plansResponse, inAppMessageResponse) -> {
                            InitialLoadBundle bundle = new InitialLoadBundle();
                            bundle.setLoyaltyTiers(userTiersResponse.getLoyaltyTiers());
                            bundle.setPlans(plansResponse.getPlans());
                            bundle.setMessage(inAppMessageResponse.getMessage());
                            return bundle;
                        });

        return updateInfo
                .filter(Boolean::booleanValue)
                .flatMap(isOk -> bundleInfo);

    }

}
