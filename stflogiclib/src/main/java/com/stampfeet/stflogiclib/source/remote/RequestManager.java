package com.stampfeet.stflogiclib.source.remote;

import com.stampfeet.stflogiclib.models.SFErrorDetailEntity;
import com.stampfeet.stflogiclib.models.SFErrorEntity;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RequestManager {

    private static final String INIT_ERROR = "Error: You must first initialize 'RequestManager'";
    private static WebServices webServices;
    private static WebServices secureWebService;
    private static String baseUrl;
    private static String appKey;
    private static Retrofit retrofit;

    public static void init(String appKey, String baseUrl) {
        RequestManager.appKey = appKey;
        RequestManager.baseUrl = baseUrl;
    }

    private static OkHttpClient getOkHttpClient(String token) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);

        builder.addInterceptor(chain -> {
            Request.Builder requestBuilder = chain.request().newBuilder();
            requestBuilder.addHeader("appKey", appKey);
            if (token != null) requestBuilder.addHeader("sessionKey", token);
            return chain.proceed(requestBuilder.build());
        });

        return builder.build();
    }

    public static WebServices getApi() {
        if (baseUrl == null) throw new RuntimeException(INIT_ERROR);

        if (webServices == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(getOkHttpClient(null))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            retrofit.create(WebServices.class);
        }
        return webServices;
        //return getMockWS();
    }

    /**
     * This method should be called after every LOGIN to update the @token
     *
     * @param token
     */
    public static void buildSecureApi(String token) {
        if (baseUrl == null) throw new RuntimeException(INIT_ERROR);

        secureWebService = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(getOkHttpClient(token))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WebServices.class);
    }

    public static void destroySecureApi() {
        secureWebService = null;
    }

    public static WebServices getSecureApi() {
        if (baseUrl == null) throw new RuntimeException("You must login first");
        return secureWebService;
    }

    public static SFErrorDetailEntity parseError(ResponseBody errorBody) {

        SFErrorEntity error = null;
        SFErrorDetailEntity errorDetail = null;

        Converter<ResponseBody, SFErrorEntity> converter = retrofit.responseBodyConverter(SFErrorEntity.class, new Annotation[0]);

        try {
            error = converter.convert(errorBody);
            errorDetail = error.getErrorDetail();
        } catch (IOException e) {
            errorDetail = new SFErrorDetailEntity(99, e.getMessage() + "");
        }

        return errorDetail;
    }
}
