package com.stampfeet.stflogiclib.source.remote;

import com.stampfeet.stflogiclib.models.Feedback;
import com.stampfeet.stflogiclib.models.User;
import com.stampfeet.stflogiclib.models.request.ChangePasswordRequest;
import com.stampfeet.stflogiclib.models.request.CreditUpdateRequest;
import com.stampfeet.stflogiclib.models.request.ForgotPasswordRequest;
import com.stampfeet.stflogiclib.models.request.GiftCardRedeemRequest;
import com.stampfeet.stflogiclib.models.request.LoginRequest;
import com.stampfeet.stflogiclib.models.request.MessageToBusinessRequest;
import com.stampfeet.stflogiclib.models.request.UpdateDeviceRequest;
import com.stampfeet.stflogiclib.models.response.InAppMessageResponse;
import com.stampfeet.stflogiclib.models.response.BannersResponse;
import com.stampfeet.stflogiclib.models.response.BaseResponse;
import com.stampfeet.stflogiclib.models.response.CreditUpdateResponse;
import com.stampfeet.stflogiclib.models.response.FeedbackPropertiesResponse;
import com.stampfeet.stflogiclib.models.response.GiftCardRedeemResponse;
import com.stampfeet.stflogiclib.models.response.LocationsResponse;
import com.stampfeet.stflogiclib.models.response.PlansResponse;
import com.stampfeet.stflogiclib.models.response.PromotionsResponse;
import com.stampfeet.stflogiclib.models.response.UserAuthResponse;
import com.stampfeet.stflogiclib.models.response.UserTiersResponse;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WebServices {

    //USER AUTHENTIFICATION

    @POST(Urls.SIGN_UP)
    Observable<UserAuthResponse> signup(
            @Body User body);

    @POST(Urls.LOGIN)
    Observable<UserAuthResponse> login(
            @Body LoginRequest body);

    @POST(Urls.FORGOT_PASSWORD)
    Observable<BaseResponse> forgotPassword(
            @Body ForgotPasswordRequest body);

    @POST(Urls.CHANGE_PASSWORD)
    Observable<BaseResponse> changePassword(
            @Path(Urls.ID_USER) int userId,
            @Body ChangePasswordRequest body);

    //BANNERS

    @GET(Urls.BANNERS)
    Observable<BannersResponse> getBanners(
            @Path(Urls.ID_USER) int userId);

    //LOCATIONS

    //    TODO: NO AUTH
    @GET(Urls.LOCATIONS)
    Observable<LocationsResponse> getLocations(
            @Query("latitude") double latitude,
            @Query("longitude") double longitude,
            @Query("pageNumber") int pageNumber,
            @Query("pageSize") int pageSize,
            @Query("orderBy") String orderBy,
            @Query("sortAscending") boolean sortAscending);

    //PLANS

    @GET(Urls.PLANS)
    Observable<PlansResponse> getUserPlans(
            @Path(Urls.ID_USER) int userId);

    //PROMOTIONS

    @GET(Urls.PROMOTIONS)
    Observable<PromotionsResponse> getPromotions(
            @Path(Urls.ID_USER) int userId,
            @Query("latitude") double latitude,
            @Query("longitude") double longitude);


    @POST(Urls.REDEEM)
    Observable<GiftCardRedeemResponse> redeemGiftCard(
            @Path(Urls.ID_USER) int userId,
            @Body GiftCardRedeemRequest body);


    //USER DATA

    @GET(Urls.USER_TIERS)
    Observable<UserTiersResponse> getUserTiers(
            @Path(Urls.ID_USER) int userId);

    @GET(Urls.IN_APP_MESSAGES)
    Observable<InAppMessageResponse> inAppMessage(
            @Path(Urls.ID_USER) int userId,
            @Query("businessId") int businessId);

    @POST(Urls.UPDATE_CREDIT)
    Observable<CreditUpdateResponse> creditUpdate(
            @Path(Urls.ID_USER) int userId,
            @Body CreditUpdateRequest body);

    @PUT(Urls.UPDATE_DEVICE)
    Observable<BaseResponse> updateDevice(
            @Path(Urls.ID_USER) int userId,
            @Body UpdateDeviceRequest body);

    @PUT(Urls.UPDATE_LOCATION)
    Observable<BaseResponse> updateLocation(
            @Path(Urls.ID_USER) int userId,
            @Body HashMap<String, Object> body);


//    TODO: EXTERNAL_CARD [POST , GET]


    //USER LOGIN DATA

    @GET(Urls.USER)
    Observable<UserAuthResponse> getUser(
            @Path(Urls.ID_USER) int userId);

    @PATCH(Urls.USER)
    Observable<BaseResponse> updateUser(
            @Path(Urls.ID_USER) int userId,
            @Body User body);

    //USER MESSAGE

    @POST(Urls.SEND_MESSAGE_TO_BUSINESS)
    Observable<BaseResponse> sendMessageToBusiness(
            @Body MessageToBusinessRequest body);

    @POST(Urls.ADD_FEEDBACK)
    Observable<BaseResponse> addFeedback(
            @Body Feedback body);

    @GET(Urls.FEEDBACK_PROPERTIES)
    Observable<FeedbackPropertiesResponse> getFeedbackProperties(
            @Path(Urls.ID_BUSINESS) int businessId);

    @GET(Urls.GET_FEEDBACK)
    Observable<BaseResponse> getFeedback(
            @Path(Urls.ID_STAMP) int stampId);


    //ADDRESS

    @GET(Urls.GET_ADDRESS)
    Observable<BaseResponse> getAddressByPostCode(
            @Query("postCode") String postCode);


    //FISHBOWL

    @GET(Urls.FISHBOWL_PROMOTIONS)
    Observable<BaseResponse> getFishbowlPromotions(
            @Path(Urls.ID_USER) int userId);


    //POSTS

    @GET(Urls.POSTS)
    Observable<BaseResponse> getPosts(
            @Query("pageNumber") int pageNumber,
            @Query("pageSize") int pageSize);


//    //ALOHA
//
////    TODO: NO AUTH
//    @POST("aloha/signup/")
//    Observable<UserAuthResponse> alohaSignup(
//            @Body HashMap<String, Object> parameters);
//
//    @PATCH("aloha/{userId}/")
//    Observable<BaseResponse> alohaUpdateUser(
//            @Path(Urls.ID_USER) int userId,
//            @Body HashMap<String, Object> parameters);
//
//    @GET("aloha/{userId}/getUserPlans")
//    Observable<AlohaPlansResponse> alohaGetUserPlans(
//            @Path(Urls.ID_USER) int userId);

}
